﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;


namespace ConsoleApp2

{
    public class Fib
    {
        public static void Fibonacci(int n)
        {
            long first = 0, second = 1, sum = 0;

            Write("{0} ", first);
            Write("{0} ", second);

            for (int i = n; i > 1; i--)
            {
                sum = first + second;

                Write("{0} ", sum);

                first = second;
                second = sum;
            }
        }
    }
   
    class Program
    {
        static void Main(string[] args)
        {
            int n; bool ok;
            do
            {
                Write("Enter n - ");

                ok = int.TryParse(ReadLine(), out n);

                if (!ok || n > 90|| n < 2)
                    WriteLine("Error!");

            } while (!ok || n > 90 || n < 2);
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Fib.Fibonacci(n);
            stopwatch.Stop();
            TimeSpan ts = stopwatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds);
            WriteLine("\nRunTime " + elapsedTime);
            ReadLine();
        }
    }
}
