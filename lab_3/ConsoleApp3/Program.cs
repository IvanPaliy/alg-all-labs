﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
namespace ConsoleApp3
{
    class Program
    {
        public class Sort
        {         
            public static void Sortirovka(int[] arr)
            {
                int tmp = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    for (int j = 0; j < arr.Length - 1; j++)
                    {
                        if (arr[j] < arr[j + 1])
                        {
                            tmp = arr[j];
                            arr[j] = arr[j + 1];
                            arr[j + 1] = tmp;
                        }
                    }
                }
            }
            public static void WriteArr(int[] arr)
            {
                Write("\nArr - {");
                for (int i = 0; i < arr.Length; i++)
                {
                  Write($"{arr[i]}");
                  if (i < arr.Length - 1)
                      Write(", ");
                }
                WriteLine("}");
            }
        }
        static void Main(string[] args)
        {

            int[] arr = new int[100000];
            Random random = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = random.Next(-160, 220);
                arr[i] /= 10;
            }

            Sort.WriteArr(arr);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Sort.Sortirovka(arr);
            
            stopwatch.Stop();
            

            Sort.WriteArr(arr);
       

            TimeSpan ts = stopwatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds);
            WriteLine("RunTime " + elapsedTime);
            ReadLine();
        }
    }
}
