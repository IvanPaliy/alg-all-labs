﻿using System;
using System.Text;

namespace ConsoleAppALGOR
{
    class Program
    {
        class Rand
        {
            public const int K = 7;
            public static int a = 2147483629;
            public static int c = 2147483587;
            public static double m = Math.Pow(2, 31) - 1;
            public static int NMM = 0, MMM = 0;


            public static void LineRand(double x, int[] arr, int k = 0)
            {
                if (k < K)
                {
                    x = (a * x + c) % m;
                    k++;
                    LineRand(x, arr, k);
                    arr[k - 1] = (int)(x);
                }
                else
                {
                    return;
                }
            }



            public static void Print(int[] arr)

            {
                for (int i = 0; i < K; i++)
                {
                    Console.WriteLine($"{i + 1} - {arr[i]} ");
                }
            }

        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            double x = 48600;
            int[] arr = new int[Rand.K];

            //створення рандома
            Rand.LineRand(x, arr);
            Rand.Print(arr);
        }

    }
}
