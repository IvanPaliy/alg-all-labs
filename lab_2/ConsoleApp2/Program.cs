﻿using System;
using System.Text;
using static System.Console;
using System.Threading;

namespace ConsoleApp1
{
    static class Random
    {
        public const int K = 30000;
        public static int a = 65539;
        public static int c = 0;
        public static double m = Math.Pow(2, 32);
        public static void Randomise(double x, int[] arr, int k = 0)
        {
            if (k < K)
            {
                x = (a * x + c) % m;
                k++;
                Randomise(x, arr, k);
                arr[k - 1] = (int)(0 + (x % (200 + 1 - 0)));
            }
        }
        public static void PrintRandom(int[] arr, int amount = K)

        {
            for (int i = 0; i < amount; i++)
                WriteLine($"{i + 1} - {arr[i]} ");
            Thread.Sleep(2000);
        }

        public static void quantity(int[] arr, int[] quant, int k = 0)
        {
            double P = 1;
            for (int i = 0; i <= 200; i++)
            {
                k = 0;
                for (int j = 0; j < K; j++)
                {
                    if (i == arr[j])
                        k++;
                    quant[i] = k;
                    P = 1.0 * quant[i] / K;
                }
                WriteLine($"Число {i} зустрiчається {quant[i]} разiв, ймовiрнiсть  = {P}");

            }
        }

        public static double[] statistic(int[] arr, double[] P, int[] quant, int k = 0)
        {
            for (int i = 0; i <= 200; i++)
            {
                k = 0;
                for (int j = 0; j < K; j++)
                    if (i == arr[j])
                        k++;
                quant[i] = k;
                P[i] = 1.0 * quant[i] / K;
                WriteLine($"статична ймовiрнiсть числа {i}  =  {P[i]}");
            }
            return P;
        }
        public static double mathexpectation(int[] arr, double[] P, double m = 0)
        {
            for (int i = 0; i < K; i++)
                m += (double)(arr[i] * P[i]);
            return m;
        }

        public static double dispersion(int[] arr, double m, double[] P, double D = 0)
        {

            for (int i = 0; i < K; i++)
                D += Math.Pow((arr[i] - m), 2) * P[i];

            return D;
        }

        public static double deviation(double D)
        {
            return Math.Sqrt(D);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            double x = 1;
            int[] arr = new int[Random.K];

            Random.Randomise(x, arr);
            Random.PrintRandom(arr, 200);


            int[] quantity = new int[Random.K];
            Random.quantity(arr, quantity);


            double[] P = new double[Random.K];
            P = Random.statistic(arr, P, quantity);


            double m = Random.mathexpectation(arr, P);
            WriteLine($"Математичне сподiвання = {m}");


            double D = Random.dispersion(arr, m, P);
            WriteLine($"Дисперсiя = {D}");


            double d = Random.deviation(D);
            WriteLine($"Середньоквадратичне вiдхилення = {d}");
        }
    }
}

