﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;

namespace ReversePolishCount
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            while (true)
            {
                try
                {
                    Console.Write("Введіть вираз: ");
                    Console.WriteLine(ReversePolishCounter.Calculate(Console.ReadLine()));
                }
                catch
                {
                    Console.WriteLine("Помилка. Перевірте правильність введення даних, формат наступний: a b ... action1 action2 action3");
                }
            }
        }
    }
}