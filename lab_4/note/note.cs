﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace note
{
    public class note<T>
    {
        public note(T data)
        {
            Data = data;
        }
        public T Data { get; set; }
        public note<T> Previous { get; set; }
        public note<T> Next { get; set; }
    }
}