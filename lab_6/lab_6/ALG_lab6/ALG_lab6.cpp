﻿#include <iostream>
#include <stdlib.h> 
#include <time.h>
#include <windows.h>
#include <chrono>
#define MAX_N 100000

void ShellSort(double arr[], int n);

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int n;
    double arr[MAX_N];
    printf("Введіть розмір масиву:"); scanf_s("%d", &n);
    printf("\nArr:\n");
    for (int i = 0; i < n; i++)
    {
        
            arr[i] = (rand() % (100 - (-1000) + 1)) + (-1000);
        arr[i] /= 10.0;
        printf("%.1lf  ", arr[i]);
    }
    printf("\n\n");

    srand(time(0));
    auto s = std::chrono::high_resolution_clock::now();
    ShellSort(arr, n);
    auto e = std::chrono::high_resolution_clock::now();
    int finish = std::chrono::duration_cast<std::chrono::nanoseconds>(e - s).count();
    printf("Sorted arr:\n");
    for (int i = 0; i < n; i++)
    {
        printf("%.1lf  ", arr[i]);
    }
    printf("\n\n");
    printf("Час виконання: %d ns", finish);
}

void ShellSort(double arr[], int n) {

 

        int step = n / 2;
    while (step <= n / 2) {
        step = 2 * step - 1;
    }
    while (step > 0)
    {
       

            for (int i = 0; i < (n - step); i++)
            {
                int j = i;
                while (j >= 0 && arr[j] > arr[j + step])
                {
                    double c = arr[j];
                    arr[j] = arr[j + step];
                    arr[j + step] = c;
                    j--;
                }
            }
        step = step / 2;
    }
}