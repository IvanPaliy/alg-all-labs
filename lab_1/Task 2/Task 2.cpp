﻿#include <iostream>

union n
{
    signed short a;
    struct num
    {
        unsigned short x0 : 1;
        unsigned short x1 : 1;
        unsigned short x2 : 1;
        unsigned short x3 : 1;
        unsigned short x4 : 1;
        unsigned short x5 : 1;
        unsigned short x6 : 1;
        unsigned short x7 : 1;
        unsigned short x8 : 1;
        unsigned short x9 : 1;
        unsigned short x10 : 1;
        unsigned short x11 : 1;
        unsigned short x12 : 1;
        unsigned short x13 : 1;
        unsigned short x14 : 1;
        unsigned short x15 : 1;
    }x;
}z;

void main()
{
    signed short x;
    printf("Enter count: ");
    scanf_s("%d", &x);
    printf("1. Data structures and associations\n");
    z.a = x;
    if (z.x.x15 == 1)
    {
        printf("Number sign: -\n");
    }
    else
    {
        printf("Number sign: +\n");
    }
    printf("2. Bitwise logical operations\n");
    if (z.x.x15 != 0 && z.x.x15 > 0)
    {
        printf("Number sign: -\n");
    }
    else if (z.x.x15 == 0 || z.x.x15 < 1)
    { 
        printf("Number sign: +\n");
    }
}

