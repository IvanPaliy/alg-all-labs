﻿#include <iostream>
#include <time.h>
struct date
{
	unsigned short WeekDay : 3;
	unsigned short MonthDay : 6;
	unsigned short Month : 4;
	unsigned short Year : 11;
	unsigned short Hours : 5;
	unsigned short Minutes : 6;
	unsigned short Seconds : 6;
};


int main()
{
	date d = { 1, 20, 10, 2015, 13, 52, 12 };
	printf("Date: Hour: %d, Minutes: %d, Second: %d, Year: %d, Month: %d, MonthDay: %d, Weekday: %d\n", d.Hours, d.Minutes, d.Seconds, d.Year, d.Month, d.MonthDay, d.WeekDay);
	return 0;
}
